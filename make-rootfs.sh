#!/bin/sh

set -euo pipefail

program="${1?}"
shell="${2?}"

# Create temporary directory
chmod +w .
mkdir rootfs

mkdir rootfs/bin
cp -L "$shell" rootfs/bin/sh

# Copy all dependencies (sent to STDIN)
while IFS="" read -r line; do
  mkdir -p "rootfs/$(dirname "$line")"
  echo "Copying $line"
  cp -Tr "$line" "rootfs/$line"
done

# Install program as /run
install -m755 "$program" rootfs/run

# Create tarball of rootfs
echo "Creating tarball..."
chmod +w "$out/root.tar.xz"
tar -C rootfs -cJvf "$out/root.tar.xz" .
