use anyhow::{Context, Result};
use rooter::with_namespace;
use tar::Archive;
use tempfile::TempDir;
use xz2::read::XzDecoder;

use std::{
    env,
    fs,
    io,
    os::unix::fs::PermissionsExt,
    process::Command,
};

const ROOT: &[u8] = include_bytes!("../root.tar.xz");

fn real_main() -> Result<()> {
    let rootfs = TempDir::new()?;
    let rootfs_path = rootfs.path();

    let decoded = XzDecoder::new(ROOT);
    let mut tar = Archive::new(decoded);

    for entry in tar.entries()? {
        let mut entry = entry?;

        if entry.unpack_in(rootfs_path).context("Unpacking tar entry")? {
            // Add write permission to directories
            let unpacked = rootfs_path.join(entry.path()?);

            let data = fs::symlink_metadata(&unpacked)?;
            if data.is_dir() {
                let mode = entry.header().mode()?;
                fs::set_permissions(unpacked, fs::Permissions::from_mode(mode | 0o200)).context("Setting permissions")?;
            }
        }
    }

    with_namespace(1024 * 1024, |mut rooter| -> Result<()> {
        // Bind all entries
        for dirent in rootfs_path.read_dir()? {
            let dirent = dirent?;
            rooter.bind(dirent.path(), dirent.file_name())?;
        }

        // Bind all host directories unless they are already bound
        for dirent in fs::read_dir("/")? {
            let dirent = dirent?;
            if let Err(err) = rooter.bind(dirent.path(), dirent.file_name()) {
                // Filter out Already Exists errors
                if !err.root_cause().downcast_ref::<io::Error>()
                        .map_or(false, |err| err.kind() == io::ErrorKind::AlreadyExists) {
                    return Err(err);
                }
            }
        }

        // Run chroot
        rooter.preserve_cwd(true);
        rooter.chroot()?;

        env::set_var("SHELL", "/bin/sh");

        // Run init script
        Command::new("/run")
            .args(env::args_os().skip(1))
            .status()
            .context("Executing /run")?;

        Ok(())
    })??;
    Ok(())
}

#[no_mangle]
pub extern "C" fn main() -> nix::libc::c_int {
    match real_main() {
        Ok(()) => return 0,
        Err(err) => {
            println!("Error: {}", err);
            return 1;
        },
    }
}
