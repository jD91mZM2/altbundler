{
  description = "An alternative nix bundler";

  inputs = {
    naersk.url = "github:nmattia/naersk";
    utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, naersk, utils }: rec {
    bundlers.altbundler = { program, system }: let
      pkgs = nixpkgs.legacyPackages."${system}";

      dependencies = pkgs.writeReferencesToFile program;

      trivialDependencies = with pkgs; [ busybox ];

      wrappedProgram = pkgs.runCommand "bundled-program" {
        nativeBuildInputs = [ pkgs.makeWrapper ];
      } ''
        makeWrapper "${program}" "$out" --prefix PATH : "${pkgs.lib.makeBinPath trivialDependencies}"
      '';

      source = pkgs.runCommand "bundled-source" {} ''
        # Copy rust code to output
        cp -ar "${./.}" "$out"

        # Build rootfs
        "${./make-rootfs.sh}" "${wrappedProgram}" "${pkgs.runtimeShell}" < "${dependencies}"
      '';

      naersk-lib = pkgs.callPackage naersk {
        # rustc with musl target
        rustc = pkgs.pkgsStatic.buildPackages.rustc;
      };
    in naersk-lib.buildPackage {
      pname = "bundled";
      src = source;

      cargoBuildOptions = opts: opts ++ [ "--target" "x86_64-unknown-linux-musl" ];

      # For some reason, just --target x86_64-unknown-linux-musl doesn't seem
      # to work - it creates a dynamic binary with zero dependencies, but a
      # dynamic one none-the-less. This works around that by being a static
      # library that we link.

      overrideMain = attrs: {
        nativeBuildInputs = attrs.nativeBuildInputs ++ (with pkgs; [
          musl
          gcc
        ]);
        installPhase = ''
          musl-gcc target/x86_64-unknown-linux-musl/release/libaltbundler.a -static -o "$out"
        '';
      };
    };

    defaultBundler = bundlers.altbundler;
  };
}
