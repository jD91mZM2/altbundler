# An alternative Nix bundler

A simple alternative to the awesome
[nix-bundle](https://github.com/matthewbauer/nix-bundle). It's mostly a
learning project - don't kill me if it fails.

## Usage

Simply pass `--bundler gitlab:jD91mZM2/altbundler` like this:

``` sh
# To bundle GNU Hello
nix bundle --bundler gitlab:jD91mZM2/altbundler nixpkgs#hello

# To bundle a bash instance and explore the chroot
nix bundle --bundler gitlab:jD91mZM2/altbundler nixpkgs#bash

# Firefox? Sure
nix bundle --bundler gitlab:jD91mZM2/altbundler nixpkgs#firefox

# Maybe bundle your neovim setup?
nix bundle --bundler gitlab:jD91mZM2/altbundler gitlab:jD91mZM2/nix-exprs#neovim
```

## Halp why is it building rustc

Unfortunately, altbundler needs to depend on a build of rustc with musl support
for static linking. I tried to avoid it, but, did not succeed.

You *may* find help in my cachix cache:

``` sh
cachix use jd91mzm2
```

## Implementation

Behind the scenes, the binary you execute will extract all the necessary nix
derivations to a temporary directory and chroot into it. This is possible using
my [rooter](https://gitlab.com/jD91mZM2/rooter) library and Linux
Namespaces:tm: :)
